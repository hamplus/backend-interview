# Hamsaa Backend Interview

## Description 

We have created a base project here containing the necessary functionalities, tests and other details that you need to fulfill the task. 



## Your task

There is an open issue describing your task. We want you to start working on it by forking to a **private** repository and start committing on `feature/campaign`.     
Once you have finished the task, send a merge request to `develop` (your fork) and assign us for code review. 

You need to give us enough permissions on your repository so we can review your code.    

We might have some discussions on parts of the code so we expect you to participate on the discussions.


Best of luck!